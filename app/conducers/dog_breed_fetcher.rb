class DogBreedFetcher
  attr_reader :breed

  def initialize(name=url)
    if name != nil 
      @nameurl = name["id"]
    else
      @nameurl = name
    end
  end

  def fetch
    @breedNames = fetch_name["message"]
  end

  def fetchImg    
    @breedImgs = fetch_info["message"]
  end

  def self.fetch(name=nil)    
    DogBreedFetcher.new(name).fetch
  end

  def self.fetchImg(name=url)
    DogBreedFetcher.new(name).fetchImg
  end

private
  def fetch_info
      JSON.parse(RestClient.get("https://dog.ceo/api/breed/#{@nameurl}/images/random").body)
  end

  def fetch_name
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list").body)
    rescue Object => e
      default_body
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end
end
