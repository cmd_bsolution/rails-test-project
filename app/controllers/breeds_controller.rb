class BreedsController < ApplicationController

  def index
    @breedNames = DogBreedFetcher.fetch
  end

  def show
    @breedImgs = DogBreedFetcher.fetchImg(url = params)
  end

end
